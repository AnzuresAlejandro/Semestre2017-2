﻿--OLAP window function Method with DISINCT().
SELECT L_LINESTATUS, L_RETURNFLAG, (CASE WHEN Y <> 0 THEN X/Y ELSE NULL END) AS pct 
FROM
(SELECT DISTINCT L_LINESTATUS, L_RETURNFLAG, 
	sum(L_QUANTITY) OVER (PARTITION BY L_LINESTATUS, L_RETURNFLAG) AS X,
	sum(L_QUANTITY) OVER (PARTITION BY L_LINESTATUS) AS Y
 FROM LINEITEM) foo;
 
--OLAP window function Method with row_number().
SELECT L_LINESTATUS, L_RETURNFLAG, (CASE WHEN Y <> 0 THEN X/Y ELSE NULL END) AS pct 
FROM
	(SELECT L_LINESTATUS, L_RETURNFLAG, 
		sum(L_QUANTITY) OVER (PARTITION BY L_LINESTATUS, L_RETURNFLAG) AS X,
		sum(L_QUANTITY) OVER (PARTITION BY L_LINESTATUS) AS Y,
		row_number() OVER (PARTITION BY L_LINESTATUS,L_RETURNFLAG) AS rnumber
		FROM 
		LINEITEM) foo WHERE rnumber = 1;

-- The GROUP-BY Method
--Se crea la tabla Findv. Tarda 2.3 sec
SELECT L_LINESTATUS, L_RETURNFLAG, sum(L_QUANTITY) AS L_QUANTITY
INTO FI FROM LINEITEM
GROUP BY L_LINESTATUS,L_RETURNFLAG;
--Se crea la tabla Ftotal. Tarda 12 msec
SELECT L_LINESTATUS, sum(L_QUANTITY) AS L_QUANTITY
INTO FT
FROM  FI
GROUP BY L_LINESTATUS;
--Se crea la tabla Fpct. Tarda 26 msec
SELECT FI.L_LINESTATUS,FI.L_RETURNFLAG ,(CASE WHEN FT.L_QUANTITY <> 0 THEN FI.L_QUANTITY/FT.L_QUANTITY
ELSE NULL END) AS pct
INTO Fpct
FROM FT JOIN FI
ON FT.L_LINESTATUS = FI.L_LINESTATUS;

-- Tabla Findv. Tarda 11 ms.
SELECT * FROM FI;
--Tabla Ftotal. tarda 12 ms.
SELECT * FROM FT;
-- Tabla Fptc. Tarda 13 ms
SELECT * FROM FPCT;
-- Se borran las tablas
DROP TABLE FI;
DROP TABLE FT;
DROP TABLE FPCT;