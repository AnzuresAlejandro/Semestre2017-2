﻿CREATE OR REPLACE FUNCTION numeros(N INTEGER)
RETURNS TEXT AS
$BODY$
DECLARE
  _acumulador TEXT := '';
  _i INTEGER := 0;
BEGIN
  
    WHILE _i <= N LOOP
	_acumulador := _acumulador || _i::TEXT || ' ';
	_i := _i + 1;
    END LOOP; 

    RETURN _acumulador;
END;
$BODY$    
LANGUAGE 'plpgsql' VOLATILE
COST 100;

-- LLAMA A LA FUNCION CON PARAMETRO
SELECT numeros(20);

-- ELIMINA LA FUNCION
-- DROP FUNCTION numeros(N INTEGER);