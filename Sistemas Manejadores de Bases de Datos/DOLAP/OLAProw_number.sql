﻿--OLAP window function Method with row_number().

DROP TABLE if EXISTS OLAProw_number;
CREATE TABLE OLAProw_number(
	Iteracion int,
	Tinicial timestamp,
	Tfinal timestamp,
	Tejecucion float
	);
	
-- FUNCION	
DROP FUNCTION if EXISTS OLAProw_number();
CREATE OR REPLACE FUNCTION OLAProw_number()
  RETURNS TABLE(Iteracion int, Tinicial timestamp, Tfinal timestamp, Tejecucion FLOAT) AS 
$BODY$
DECLARE 
  iter INTEGER := 1;
BEGIN
   WHILE iter <= 5 LOOP
	INSERT INTO OLAProw_number(Iteracion, Tinicial) VALUES(iter, CURRENT_TIMESTAMP);
	SELECT L_LINESTATUS, L_RETURNFLAG, (CASE WHEN Y <> 0 THEN X/Y ELSE NULL END) AS pct 
	FROM
	(SELECT L_LINESTATUS, L_RETURNFLAG, 
		sum(L_QUANTITY) OVER (PARTITION BY L_LINESTATUS, L_RETURNFLAG) AS X,
		sum(L_QUANTITY) OVER (PARTITION BY L_LINESTATUS) AS Y,
		row_number() OVER (PARTITION BY L_LINESTATUS,L_RETURNFLAG) AS rnumber
		FROM 
		LINEITEM) foo WHERE rnumber = 1;
	UPDATE OLAProw_number SET Tfinal = CURRENT_TIMESTAMP WHERE Iteracion = iter;
	UPDATE OLAProw_numer SET Tejecucion = cast(DATEDIFF(ms, Tinicial, Tfinal) as float)/1000 where Iteracion = iter;
	iter := iter + 1;
   END LOOP;
END   
$BODY$    
LANGUAGE 'plpgsql' VOLATILE
COST 100;

-- SE MUESTRA LA TABLA
SELECT * FROM OLAProw_number;
--SELECT avg(Tejecucion) as TPEjecucion FROM OLAProw_number;
