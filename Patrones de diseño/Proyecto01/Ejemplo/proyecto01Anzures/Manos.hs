module Manos where
import Carta
import Data.List

--baraja :: [Carta]
--baraja = [(Carta palo valor)| palo <-[Pica,Trebol,Diamante,Corazon], valor <- [Dos,Tres,Cuatro,Cinco,Seis,Siete,Ocho,Nueve,Diez,Joker,Reina,Rey,Ace] ]

suc:: ValorNumerico -> ValorNumerico
suc x= case x of
	Dos -> Tres
	Tres -> Cuatro
	Cuatro -> Cinco
	Cinco -> Seis
	Seis -> Siete
	Siete -> Ocho
	Ocho -> Nueve
	Nueve -> Diez
	Diez -> Joker
	Joker -> Reina
	Reina -> Rey
	Rey -> Ace
	Ace -> Dos

data ValorMano = CartaAlta Mano | Par Mano | DoblePar Mano | Trio Mano| Escalera  Mano | Color Mano | Full Mano | Poker Mano| EscaleraDeColor Mano deriving (Eq,Ord,Read,Show)

-- Funcion que regresa si hay un par o un tercio o un poker 
-- ya que compara la longitud  una lista de posiles jugadas 
tieneCartasSemejantes:: Int->[[Carta]] -> Bool
tieneCartasSemejantes _ []  = False
tieneCartasSemejantes num (x:xs) = if ( num == (length x))
							then True
							else  tieneCartasSemejantes num xs 

-- Funcion que regresa si hay un par o un tercio o un poker 
-- ya que compara la longitud  una lista de posiles jugadas 
-- parecida a la anterior pero regresa por ejemplo (tieneMasCartasSemejantes 2 mano) regresa true si hay 
-- Pares o Triples o Full etc en la otra solo devolvia true si habia de longitud 2 
tieneMasCartasSemejantes:: Int->[[Carta]] -> Bool
tieneMasCartasSemejantes _ []  = False
tieneMasCartasSemejantes num (x:xs) = if ( num <= (length x))
							then True
							else  tieneCartasSemejantes num xs 
-- Funcion auxiliar que permite saber si tenemos una sublista de cartas con 5 elementos util para saber si tenemos una escalera (que solo es valida si tenemos 5 cartas consecutivas)
--  y tambien si tenemos color ( solo cuenta si tenemos 5 cartas del mismo palo)
tiene5:: [[Carta]] -> Bool
tiene5 []  = False
tiene5 (x:xs) = if ( 5 <= (length x))
							then True
							else  tiene5 xs 
-- Aplana una lista de listas							
aplana:: [[Carta]] -> [Carta]
aplana cartas = [ y | x <- cartas, y <- x] 

-- Funcion mas importante del programa ya que decide al ganador gracias a las comparaciones 
-- decide si es factible ir o dejar la mano por parte de la maquina 
creaManos::Mano -> ValorMano
creaManos [] = (CartaAlta [])
creaManos mano
			| (and ((tieneCartasSemejantes 2 manoPares) : (not (tieneMasCartasSemejantes 3 manoPares))  : (not (tiene5 manoPalo)) : (not (tiene5 manoEscalera)):[])) = 
				(if (cuentaPares manoPares >= 2)
					then (DoblePar (take 5 (aplana (ordenImportancia manoPares))))
					else (Par (take 5 (aplana (ordenImportancia manoPares)))))
			| (and ((tieneCartasSemejantes 3 manoPares) : (not (tieneMasCartasSemejantes 4 manoPares))  : (not (tiene5 manoPalo)) : (not (tiene5 manoEscalera)):[] )) =
				(let manoOrdenada = ordenImportancia manoPares;
					 manoAux = ordenImportancia (ordenaManoIgualValor (take 5 (aplana manoOrdenada)))
					in ( if ((length (head (tail manoAux))) == 2) 
						then (Full (aplana manoAux))
						else (Trio (aplana manoAux))))
			| (and (((tiene5 manoPalo)):(not (tiene5 manoEscalera)):[])) = (Color (take 5 (aplana(ordenImportancia manoPalo))))
			| (tieneCartasSemejantes 4 manoPares)  = (Poker (take 5 (aplana(ordenImportancia manoPares))))			
			| (and ((not (tiene5 manoPalo)): (tiene5 manoEscalera) :[])) = (Escalera (take 5 (aplana(ordenImportancia manoEscalera))))	
			| (and ((tiene5 manoEscalera) : (tiene5 manoEscalera) : [] )) = (EscaleraDeColor (take 5 (aplana(ordenImportancia manoEscalera))))	
			| otherwise =  CartaAlta (reverse (sort mano))
			where manoPares = ordenaManoIgualValor mano;
				  manoPalo = ordenaManoMismoPalo mano;
				  manoEscalera = ordenaManoEscalera mano

-- compara el valor de 2 cartas
comparaRango:: [Carta] -> [Carta] -> Bool
comparaRango _ [] = False
comparaRango [] y = True
comparaRango (x:xs) (y:ys) = (valor x) > (valor y)
-- ordenacion de las sublistas igual al insert de la clase Data.List
ordenasubLista:: [Carta] -> [[Carta]] -> [[Carta]]
ordenasubLista e [] = [e]
ordenasubLista e (x:xs)
			| comparaRango e x = e:x:xs
			| otherwise = x: ordenasubLista e xs
-- PArecido al insertSort
ordenasubListaDeCartas:: [[Carta]] -> [[Carta]]
ordenasubListaDeCartas [] = []
ordenasubListaDeCartas (x:xs) = ordenasubLista x (ordenasubListaDeCartas xs)
-- Ordena cartas por sublistas para saber cual es la jugada mas grande de nuestra mano
ordenaCartas:: [Carta] -> [[Carta]] -> [[Carta]]
ordenaCartas e [] = [e]
ordenaCartas e (x:xs)
			| ((length e) >= (length x)) = e:x:xs
			| otherwise = x: ordenaCartas e xs

-- OrdenaCartas parecido a insertSort
ordenCartas:: [[Carta]] -> [[Carta]]
ordenCartas [] = []
ordenCartas (x:xs) = ordenaCartas x (ordenCartas xs)


ordenImportancia:: [[Carta]] -> [[Carta]]
ordenImportancia cartas = ordenCartas cartas--ordenCartas (ordenCartas cartas)


cuentaPares:: [[Carta]] -> Int
cuentaPares [] = 0
cuentaPares (x:xs) =if ((length x) == 2)
						then (1 + cuentaPares xs)
						else cuentaPares xs


ordenaManoIgualValor:: [Carta] -> [[Carta]]
ordenaManoIgualValor cartas= agrupaIgualValor (reverse (sort cartas))

cartasMismoPalo:: Carta -> Carta -> Bool
cartasMismoPalo carta1 carta2= (palo carta1) == (palo carta2)


cartasEscalera::Carta -> Carta -> Bool
cartasEscalera carta1 carta2= if ((valor carta1) == (suc (valor carta2))) 
									then 
										True
									else if ( (valor carta2) == (suc (valor carta1)) ) 
											then True
											else False 

agrupaEscalera:: [Carta] -> [[Carta]]
agrupaEscalera (x:[]) = [[x]]
agrupaEscalera (x:xs) = ([(x : takeWhile (cartasEscalera x) xs)] ++ (if ((dropWhile (cartasEscalera x) xs) == []) then [] else agrupaEscalera (dropWhile (cartasEscalera x) xs))) 

ordenaManoEscalera:: [Carta] -> [[Carta]]
ordenaManoEscalera cartas = agrupaEscalera (reverse (sort cartas))

insertAux:: Carta -> [Carta] ->[Carta]
insertAux e [] = [e]
insertAux e (x:xs)
			| cartasMismoPalo e x = e:x:xs
			| otherwise = x: insertAux e xs

insertSort::[Carta] ->[Carta]
insertSort [] = []
insertSort (x:xs) = insertAux x (insertSort xs)

ordenaManoMismoPalo:: [Carta] -> [[Carta]]
ordenaManoMismoPalo cartas = agrupaIgualPalo (reverse (insertSort cartas))

cartasIguales:: Carta -> Carta -> Bool
cartasIguales carta1 carta2= (valor carta1) == (valor carta2)

agrupaIgualPalo:: [Carta] -> [[Carta]]
agrupaIgualPalo (x:[]) = [[x]]
agrupaIgualPalo (x:xs) = ([(x : takeWhile (cartasMismoPalo x) xs)] ++ (if ((dropWhile (cartasMismoPalo x) xs) == []) then [] else agrupaIgualPalo (dropWhile (cartasMismoPalo x) xs))) 

agrupaIgualValor:: [Carta] -> [[Carta]]
agrupaIgualValor (x:[]) = [[x]]
agrupaIgualValor (x:xs) = ([(x : takeWhile (cartasIguales x) xs)] ++ (if ((dropWhile (cartasIguales x) xs) == []) then [] else agrupaIgualValor (dropWhile (cartasIguales x) xs))) 
