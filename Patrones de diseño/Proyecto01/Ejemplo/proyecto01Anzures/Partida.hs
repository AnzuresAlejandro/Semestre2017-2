module Partida where 
import Carta
import Data.List
import Manos
data Jugador=Jugador {nombre :: String, mano :: Mano} | Null deriving (Show,Eq)

--Funcion que crea un numero de jugadores con los que el oponente se va a enfrentar 
creaJugadores:: Int ->[Jugador] 
creaJugadores 1 = [(Jugador ("Player " ++ (show 1)) [])]
creaJugadores n = (Jugador ("Player " ++ (show n)) [] ) : creaJugadores (n - 1) 

-- Funcion para repartir cartas
-- regresa el numero de cartas  para repartir a los jugadores elimina las cartas del mazo
--Segunda version devuelve la carta y el mazo que resultan modificados al tomar la primera carta de la baraja
repartir::Int->[Carta]->([Carta],[Carta])
repartir 1 bar = let (carta,mazo) = tomaCarta 0 bar in ([carta],mazo)
repartir n bar = let (carta,mazo) = tomaCarta 0 bar;
					 (cartasRepartidas , mazoModificado) =repartir (n - 1) mazo 
					 in ((carta:cartasRepartidas), mazoModificado)
-- Metodo auxiliar para repartir cartas a los jugadores
-- regresa el jugador con las cartas modificadas 
repartirAjugador::  [Carta] -> Jugador ->Jugador
repartirAjugador cartas (Jugador x mano)= (Jugador x (cartas ++ mano)) 



-- regresa si el jugador es usuario o es parte de la "inteligencia artificial"
esUsuario:: Jugador -> Bool
esUsuario (Jugador nombre _)= if (nombre `intersect` "Player" == "Player") then True else False

-- regresa los jugadores asignadoles sus primeras cartas y regresa los jugadores con sus 
-- respectivas cartas mas el mazo ya modificado
primerasCartas:: [Jugador] -> [Carta] -> IO ([Jugador],[Carta])
primerasCartas (x:[]) baraja = 
	do
		let (mano,restBaraja)= repartir 2 baraja;temporal = repartirAjugador mano x
		
		return ([temporal],restBaraja)	

primerasCartas (x:xs) baraja= 
	do
		let (mano,restBaraja)= repartir 2 baraja; temporal = repartirAjugador mano x
		(otros,resto )<- (primerasCartas xs restBaraja)
		
		return ([temporal]++ otros, resto)	

desicionPrimerTurno:: Jugador -> IO (Jugador)
desicionPrimerTurno jugador 
		| (tieneCartasSemejantes 2 manoAux) = return jugador
		| (cartasAltas (mano jugador)) = return jugador
		| otherwise = return Null
		where manoAux = ordenaManoIgualValor (mano jugador)


cartasAltas:: [Carta] -> Bool
cartasAltas (x:xs:[])= if (and ((x > (Carta Trebol Siete)) :(xs > (Carta Trebol Siete)) :[]))
						then True else False
desicionMaquina:: Jugador -> IO (Jugador)
desicionMaquina jugador 
		| (( Par [(Carta Trebol Dos), (Carta Trebol Dos), (Carta Diamante Tres), (Carta Trebol Cuatro), (Carta Trebol Cinco)] ) <  manoAux) = return jugador
		| otherwise = return Null
		where manoAux = creaManos (mano jugador)

decisionesPrimerTurno:: Jugador -> IO (Jugador)
decisionesPrimerTurno jugador= 
	do
	if(not (esUsuario jugador))
		then 
			do
				putStrLn "Elige un numero:\n 1:Ir\n 2:Pasar"
				putStrLn (show jugador)
				x <- getLine
				let opcion= read x :: Int
				if (opcion == 1) 
					then 
						return jugador
					else 
						return Null
		else
			do
				jugador <- desicionPrimerTurno jugador
				if (jugador /= Null)

					then putStrLn (nombre jugador)
					--then putStrLn (show jugador)
					else putStrLn "Retirado" 
				return jugador

decisiones:: Jugador -> IO (Jugador)
decisiones jugador= 
	do
	if(not (esUsuario jugador))
		then 
			do
				putStrLn (show jugador)
				putStrLn "Elige un numero:\n 1:Ir\n 2:Pasar"
				x <- getLine
				let opcion= read x :: Int
				if (opcion == 1) 
					then 
						return jugador
					else 
						return Null
		else
			do
				jugador <- desicionMaquina jugador
				if (jugador /= Null)
					then putStrLn (nombre jugador)
					--then putStrLn (show jugador)
					else putStrLn "Retirado" 
				return jugador


ganador:: [Jugador] -> Jugador
ganador (x:[]) = x 
ganador (x:y:xs) = if ((creaManos (mano x)) > (creaManos (mano y)))
					then ganador (x:xs)
					else ganador (y:xs)
