
import System.Random
import Data.Function
module Carta where

data ValorNumerico = Dos|Tres|Cuatro|Cinco|Seis|Siete|Ocho|Nueve|Diez|Joker|Reina|Rey|Ace
    deriving (Eq, Ord,Read)
instance Show ValorNumerico where
  show x = case x of
    Dos   -> "2"
    Tres -> "3"
    Cuatro  -> "4"
    Cinco  -> "5"
    Seis   -> "6"
    Siete -> "7"
    Ocho -> "8"
    Nueve  -> "9"
    Diez   -> "10"
    Joker  -> "J"
    Reina -> "Q"
    Rey  -> "K"
    Ace   -> "A"
data Palo = Pica| Trebol | Diamante | Corazon  deriving (Eq,Read, Ord)
instance Show Palo where
 show x = case x of
    Pica   -> "P"
    Trebol -> "T"
    Corazon  -> "C"
    Diamante  -> "D" 
data Carta = Carta {palo :: Palo , valor :: ValorNumerico} deriving (Read,Eq)
instance Ord Carta where
    compare = compare `on` valor
instance Show Carta where
    show (Carta palo valor)= ("("++(show palo)++","++(show valor)++")")    

-- Mano 
--data Mano = Mano Carta Carta Carta Carta Carta deriving (Read, Eq, Show, Ord)
type Mano =[Carta]
-- Se genera un entero entre 1 y n
genera :: Int -> IO Int
genera n = randomRIO (0::Int,n)

 
-- obtener una carta dado una posicion sera auxiliar para la funcion revolver 
obtenerCarta:: Int -> [Carta] -> Carta
obtenerCarta 0 (x:xs)= x 
obtenerCarta n (x:xs) = obtenerCarta (n - 1) xs 
-- quitar carta de la baraja
quitarCarta:: Int -> [Carta] -> [Carta]
quitarCarta 0 (x:xs)= xs 
quitarCarta n (x:xs) = (x : quitarCarta (n - 1) xs) 
-- Toma Cartas toma una carta de la baraja y devuelve la baraja sin la carta 
tomaCarta::Int -> [Carta] -> (Carta,[Carta])
tomaCarta n baraja = (obtenerCarta n baraja, quitarCarta n baraja)
-- devuelve las cartas barajedas 
revolver:: [Carta] -> IO [Carta]
revolver mazo= do
    n <- genera ((length mazo) - 1)
    if ((length mazo) > 0) 
        then 
            do 
            let (carta,mazoA) = tomaCarta n mazo
            xs <- revolver mazoA
            return (carta:xs)
        else
            return []
