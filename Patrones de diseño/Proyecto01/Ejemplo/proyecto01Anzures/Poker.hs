import Carta
import Partida
import Manos
import Data.List

baraja :: [Carta]
baraja = [(Carta palo valor)| palo <-[Pica,Trebol,Diamante,Corazon], valor <- [Dos,Tres,Cuatro,Cinco,Seis,Siete,Ocho,Nueve,Diez,Joker,Reina,Rey,Ace] ]
	
main = do 
	putStrLn "Juego de Poker"
	putStrLn "Propociona tu nombre o un identificador"
	nombre <- getLine
	putStrLn "Ingresa numero de adversarios"
	n <- getLine
	let jugador = Jugador nombre []
	let jugadores = creaJugadores (read n :: Int) ++ [Jugador nombre []]
	mazo <- revolver baraja
	ganador <- ronda jugadores 1 mazo 
	putStrLn (show ganador)	
	putStrLn $ "Fin del Juego"


-- Funcion que implementa el juego 
-- regresa al ganador de la ronda 	
-- cambia a IO Jugador


ronda:: [Jugador] -> Int -> [Carta]  -> IO Jugador
ronda jugadores 1 baraja= do
	(jugadores,mazo) <- primerasCartas jugadores baraja
	jugadoresModificados <- mapM decisionesPrimerTurno jugadores
	let jugadoresAux = [ x | x <- jugadoresModificados, (x /= Null) ]
	if (1 == (length jugadoresAux))
		then return (head jugadoresAux)
		else ronda jugadoresAux 2 mazo

ronda jugadores 2 baraja= do
	let (cartas, mazoModificado) = repartir 3 baraja; 
		jugadoresAux = map  (repartirAjugador cartas) jugadores
	jugadoresModificados <- mapM decisiones jugadoresAux
	let jugadoresAux = [ x | x <- jugadoresModificados, (x /= Null) ]
	if (1 == (length jugadoresAux))
		then return (head jugadoresAux)
		else ronda jugadoresAux 3 mazoModificado

ronda jugadores 3 baraja= do
	let (cartas, mazoModificado) = repartir 1 baraja; 
		jugadoresAux = map  (repartirAjugador cartas) jugadores
	jugadoresModificados <- mapM decisiones jugadoresAux
	let jugadoresAux = [ x | x <- jugadoresModificados, (x /= Null) ]
	if (1 == (length jugadoresAux))
		then return (head jugadoresAux)
		else ronda jugadoresAux 4 mazoModificado

ronda jugadores 4 baraja= do
	let (cartas, mazoModificado) = repartir 1 baraja; 
		jugadoresAux = map (repartirAjugador cartas) jugadores
	jugadoresModificados <- mapM decisiones jugadoresAux
	let jugadoresAux = [ x | x <- jugadoresModificados, (x /= Null) ]
	if (1 == (length jugadoresAux))
		then return (head jugadoresAux)
		else ronda jugadoresAux 5 mazoModificado

ronda jugadores _ _= do
	let campeon = (ganador jugadores)
	putStrLn $ show (creaManos (mano campeon))
	return campeon
 

