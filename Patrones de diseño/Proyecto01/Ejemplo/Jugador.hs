module Jugador where 
	import Tablero
	data Jugador = Jugador {nombre:: String, tablero :: Tablero, vidas:: Int}
	instance Show Jugador where
		show (Jugador nombre tablero vidas) = ("Jugador: "++(show nombre)++"\nVidas:"++(show vidas)++(show tablero)++"\n");
	--main::IO -> IO 
	
	creaJugadores:: String -> String -> Int -> Int -> [Jugador]
	creaJugadores nombre nombre2 n numVidas= [(Jugador nombre (creaTablero n) numVidas ) , (Jugador nombre2 (creaTablero n) numVidas)]
	proporcionaDato:: IO String
	proporcionaDato = do 
		putStrLn "Propociona dato positivo"
		n <- getLine
		if ((read n) < 0)
			then 
				proporcionaDato
			else 
				return n
	vidaJugador:: Jugador -> Int
	vidaJugador (Jugador _ _ vida) = vida
	--colocarBarcos:: [Jugador] -> [Jugador]

	colocaBarcos (Jugador nombre tablero vidas) barcos = do 
		if(barcos == 0)
			then 
				return (Jugador nombre tablero vidas)
			else 
				do
					putStrLn "Proporcionar coordenada caracter"
					r <- getLine
					putStrLn "Proporcionar coordenada numerica"
					g <- getLine
					respuesta <- casillaEnTablero (read r) (read g)
					colocaBarcos (Jugador nombre tablero vidas) (barcos - 1)

	main = do 
		putStrLn "Juego de Batalla naval"
		putStrLn "Jugador 1 : Propociona tu nombre o un identificador"
		nombre <- getLine
		putStrLn "Jugador 2 : Propociona tu nombre o un identificador"
		nombre2 <- getLine
		putStrLn "Propociona el tamaño del tablero (solo un numero dado que el tablero es un cuadrado)"
		n <- proporcionaDato 
		putStrLn "Proporciona el numero de barcos con los que se jugaran"
		r <- proporcionaDato 
		let jugadores = creaJugadores nombre nombre2 (read n) (read r) 
		putStrLn (show jugadores) 
		--let jugadores =  colocaBarcos jugadores
		putStrLn (show jugadores) 


	--tirarBomba:: Char -> Int -> IO
