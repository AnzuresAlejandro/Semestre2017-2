module Jugador where 
import Tablero
import Data.Char
data Jugador = Jugador {nombre:: String, tablero :: Tablero, vidas:: Int}
instance Show Jugador where
	show (Jugador nombre tablero vidas) = ("Jugador: "++(show nombre)++"\nVidas:"++(show vidas)++(show tablero)++"\n");
--main::IO -> IO 
creaJugadores:: String -> String -> Int -> Int -> [Jugador]
creaJugadores nombre nombre2 n numVidas= [(Jugador nombre (creaTablero n) numVidas ) , (Jugador nombre2 (creaTablero n) numVidas)]
proporcionaDato:: IO String
proporcionaDato = do 
	putStrLn "Propociona dato positivo"
	n <- getLine
	if ((read n) < 0)
		then 
			proporcionaDato
		else 
			return n
proporcionaCaracter:: IO String
proporcionaCaracter = do
	putStrLn "Propociona caracter valido"
	n<- getLine
	if('a' <= (toLower (head n)) || (toLower (head n)) <= 'n')
		then 
			proporcionaCaracter
		else 
			return n

nombreJugador :: Jugador -> String
nombreJugador (Jugador nombre _ _ ) = nombre 
vidaJugador:: Jugador -> Int
vidaJugador (Jugador _ _ vida) = vida

tableroJugador:: Jugador -> Tablero
tableroJugador (Jugador _ tab _) = tab

cambiaPosicionBarco:: String -> Int -> String -> Int -> Jugador-> Jugador
cambiaPosicionBarco coordenadaViejaCaracter coordenadaViejaNum coordenadaNuevaCaracter coordenadaNuevaNum jugador 
	| ((not (casillaOcupada (numero coordenadaViejaCaracter) coordenadaViejaNum 1 (tableroCoordenadasPropias (tableroJugador jugador)))) && (not (casillaOcupada (numero coordenadaNuevaCaracter) coordenadaNuevaNum 1 (tableroCoordenadasPropias (tableroJugador jugador)))) ) ==True = 	Jugador (nombreJugador jugador) tableroModificado (vidaJugador jugador)
	| otherwise = jugador	
	where tableroModificado = modificaTableroPropio coordenadaViejaCaracter coordenadaViejaNum 0 (modificaTableroPropio coordenadaNuevaCaracter coordenadaNuevaNum 1 (tableroJugador jugador));
		  tableroModificadoTemp = modificaTableroPropio coordenadaNuevaCaracter coordenadaNuevaNum 1 (tableroJugador jugador)


turno:: Jugador ->[Jugador] -> Bool -> Bool -> IO [Jugador] 
turno jugador jugadores realizado exito= do
	putStrLn "Mostrar menu de opciones: Elige el numero de la opcion que quieras realizar "
	if(exito == True)
		then 
			do 
				let aux = jugadores++[jugador]
				return 	aux
	else 
		do
			if(realizado == True)
				then 
					do
						opcion <- menu2 jugador 
						jugadorAux <- ejecutaOpcion jugador jugadores (snd opcion) realizado 
						turno (fst jugadorAux) (snd jugadorAux) (fst opcion) exito
				else 
					do
						opcion <- menu1 jugador
						jugadorAux <- ejecutaOpcion jugador jugadores (snd opcion) realizado
						--turno jugadorAux jugadores (fst opcion) exito
						turno (fst jugadorAux) (snd jugadorAux) (fst opcion) exito
menu1:: Jugador -> IO (Bool,Int)
menu1 jugador = do
	putStrLn  "1.- Mostrar tablero"
	putStrLn  "2.- Mover barco de posicion"
	putStrLn  "3.- tirarBomba"
	respuesta<- getLine
	if((read respuesta) > 0 && (read respuesta)< 4)
		then 
			if ((read respuesta) == 2)
				then 
					return (True,(read respuesta))
				else
					return (False,(read respuesta))
		else 
			do
				putStrLn "Opcion no valida vuelve a elejir"
				menu1 jugador

menu2:: Jugador -> IO (Bool,Int)
menu2 jugador = do
	putStrLn  "1.- Mostrar tablero"
	putStrLn  "2.- tirarBomba"
	respuesta<- getLine
	if((read respuesta) > 0 && (read respuesta)< 4)
		then 
			return (True,(read respuesta))
		else 
			do
				putStrLn "Opcion no valida vuelve a elejir"
				menu1 jugador
ejecutaOpcion:: Jugador -> [Jugador]-> Int -> Bool  ->IO (Jugador,[Jugador])
ejecutaOpcion jugador jugadores opcion realizado = 
	if (realizado)
		then 
			if(opcion == 1)
			then 
				do 
				putStrLn (show (tableroJugador jugador))
				return (jugador,jugadores)
			else 
				--tirar Bomba
				return (jugador,jugadores)
		else
			if(opcion == 1)
			then 
				do 
					putStrLn (show (tableroJugador jugador))
					return (jugador,jugadores)
			else 
				if(opcion ==2)
					then
						--Cambia de posicion un barco
						do
							putStrLn "Proporciona la posicion (letra) en la  que se encuentra"
							datoViejo <-proporcionaCaracter
							putStrLn "Proporciona la posicion (numerica) vieja"
							numeroViejo <- proporcionaDato
							if(casillaEnTablero datoViejo (read numeroViejo) (tableroJugador jugador))
								then 
									do
										putStrLn "Proporciona la coordenada (letra)  nueva"
										datoNuevo <-proporcionaCaracter
										putStrLn "Proporciona la posicion  (numerica) nueva"
										numeroNuevo <- proporcionaDato		
										if(casillaEnTablero datoNuevo (read numeroNuevo) (tableroJugador jugador))
											then
												do 
													let jugadorAux = cambiaPosicionBarco datoViejo (read numeroViejo) datoNuevo (read numeroNuevo)  jugador
													return (jugadorAux,jugadores)
											else 
												do
													putStrLn "coordenadas nuevas son incorrectas"
													ejecutaOpcion jugador jugadores opcion realizado
								else
									do
										putStrLn "coordenadas nuevas son incorrectas"
										ejecutaOpcion jugador jugadores opcion realizado
					else
						-- tirar Bomba 
						return (jugador,jugadores)

-- Funcion que interactua con el usuario con el fin de colocar sus barcos los lugares que ellos quieran 
colocaBarcos (Jugador nombre tablero vidas) barcos = do 
	if(barcos == 0)
		then do
			putStrLn "Barcos Colocados correctamente"
			return (Jugador nombre tablero vidas)
		else 
			do
				putStrLn ("Barcos Rertantes " ++ (show barcos))
				putStrLn "Proporcionar coordenada caracter"
				r <- getLine
				putStrLn "Proporcionar coordenada numerica"
				g <- getLine
				let respuesta = casillaEnTablero r (read g) tablero
				if(respuesta) then 
					if (casillaOcupada (numero r) (read g) 1 (tableroCoordenadasPropias tablero))
						then 
							do
								putStrLn "coordenada ocupada por otro barco elije otra coordenada"
								colocaBarcos (Jugador nombre tablero vidas) (barcos)
						else 
							colocaBarcos (Jugador nombre (modificaTableroPropio r (read g) 1 tablero ) vidas) (barcos - 1)
					else 
						do 
							putStrLn "coordenada invalida"
							colocaBarcos (Jugador nombre tablero vidas) (barcos)		
				


main = do 
	putStrLn "Juego de Batalla naval"
	putStrLn "Jugador 1 : Propociona tu nombre o un identificador"
	nombre <- getLine
	putStrLn "Jugador 2 : Propociona tu nombre o un identificador"
	nombre2 <- getLine
	putStrLn "Propociona el tamaño del tablero (solo un numero dado que el tablero es un cuadrado)"
	n <- proporcionaDato 
	putStrLn "Proporciona el numero de barcos con los que se jugaran"
	r <- proporcionaDato 
	let jugadores = creaJugadores nombre nombre2 (read n) (read r) 
	--putStrLn (show jugadores) 
	putStrLn "Jugador 1 Coloca tus barcos"
	jugador1 <- colocaBarcos (head jugadores) (read r)
	putStrLn (show jugador1)
	putStrLn "Jugador 2 Coloca tus barcos"
	jugador2 <- colocaBarcos (head (tail jugadores)) (read r)
 	putStrLn (show jugador2)
 	let jugadores = [jugador1]++[jugador2]
	putStrLn ("Comienza el juego el jugador: "++ (nombreJugador jugador1))




tirarBomba:: Jugador -> [Jugador] ->IO [Jugador]
tirarBomba jugador jugadores = do
	putStrLn "Propociona las coordenadas de donde quieras tirar la bomba"
	putStrLn "Proporciona la posicion (letra) en la  que se encuentra"
	dato <-proporcionaCaracter
	putStrLn "Proporciona la posicion (numerica) vieja"
	numero <- proporcionaDato
	if(casillaEnTablero dato(read numero) (tableroJugador jugador))
		then 
			do
				let cambios = tiraBomba dato (read numero) jugador jugadores
				if ((vidaJugador (head (snd (cambios)))) == (vidaJugador (head jugadores)))  
					then 
						do 
							putStrLn "Le has atinado a un submarino enemigo"
							let actualizaJugadores = (snd cambios) ++ [(fst cambios)]
							return actualizaJugadores
					else  
						do 
							putStrLn "No le has dado a ningun submarino enemigo"
							let actualizaJugadores = (snd cambios) ++ [(fst cambios)]
							return actualizaJugadores
		else 
			tirarBomba jugador jugadores

tiraBomba:: String -> Int -> Jugador -> [Jugador] -> (Jugador,[Jugador])
tiraBomba letra num jugador jugadores = (jugadorMod, jugadoresMod)
	where jugadorMod = (Jugador (nombreJugador jugador) (modificaTableroEnemigo letra num 2 (tableroJugador jugador) ) (vidaJugador jugador));
		  jugadoresMod = [(Jugador (nombreJugador oponente) (modificaTableroPropio letra num 2 (tableroJugador oponente)) vidasAux)] ++ (tail jugadores);
		  vidasAux = if (casillaOcupada (numero letra) num 1 (tableroCoordenadasPropias (tableroJugador oponente))) then (vidaJugador oponente) - 1 else (vidaJugador oponente);
		  oponente = (head jugadores)


