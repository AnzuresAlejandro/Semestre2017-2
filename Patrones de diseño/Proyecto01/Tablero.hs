module Tablero 
(Tablero
,creaTablero
,casillaOcupada
,casillaEnTablero
,tableroCoordenadasPropias
,tableroCoordenadasEnemigo
,modificaTableroPropio
,modificaTableroEnemigo
,numero) where
type Renglon = [Int]
data Tablero = Tablero {coordenadasEnemigo ::[Renglon], coordenadasPropias :: [Renglon]}
instance Show Tablero where
	show (Tablero coordenadasEnemigo coordenadasPropias) = ("\n"++(dibuja coordenadasEnemigo)++(linea ((length coordenadasEnemigo) * 2 +1) )++"\n"++(dibuja coordenadasPropias))

modificaTableroPropio:: String ->Int -> Int-> Tablero -> Tablero
modificaTableroPropio x y objeto (Tablero tabPro z) = (Tablero (colocaObjeto x y tabPro objeto) z)

modificaTableroEnemigo:: String ->Int -> Int-> Tablero -> Tablero
modificaTableroEnemigo x y objeto (Tablero tabPro z) = (Tablero tabPro (colocaObjeto x y z objeto))

casillasTablero:: Tablero -> Int
casillasTablero (Tablero x _) = (length x) 

casillaExistente:: Int -> Int -> Tablero -> Bool
casillaExistente num1 num2 tablero = 
	if (num1 <= (casillasTablero tablero) && num1 > 0 ) 
		then 
			if (num2 <= (casillasTablero tablero) && num2 > 0)
				then 
					True
				else
					False
		else 
			False
tableroCoordenadasPropias:: Tablero -> [Renglon]
tableroCoordenadasPropias (Tablero propias _) = propias
tableroCoordenadasEnemigo:: Tablero -> [Renglon]
tableroCoordenadasEnemigo (Tablero _ enemigo) = enemigo 
creaTablero:: Int -> Tablero
creaTablero n = (Tablero (creaTableroAuxiliar n n) (creaTableroAuxiliar n n)) 

creaTableroAuxiliar:: Int -> Int -> [Renglon]
creaTableroAuxiliar 1 n= [creaRenglon n] 
creaTableroAuxiliar n m = [(creaRenglon m)] ++  (creaTableroAuxiliar (n -1) m )   						

creaRenglon:: Int -> Renglon
creaRenglon 1 = [0]
creaRenglon n = [0] ++ creaRenglon (n -1)

casillaOcupada::Int -> Int -> Int -> [Renglon] -> Bool
casillaOcupada 1 y ob  tablero = buscarRenglon y ob (head tablero)
casillaOcupada n y ob tablero = casillaOcupada (n - 1) y ob (tail tablero)  

buscarRenglon:: Int-> Int -> Renglon ->Bool
buscarRenglon 1 ob renglon = if ((head renglon) == ob) then True else False
buscarRenglon y ob renglon = buscarRenglon (y - 1) ob (tail renglon)


-- Funcion importante que sirve para poder colocar objetos en el tablero del 
-- Objetos bombas o barcos 
colocaObjeto:: String -> Int -> [Renglon]-> Int -> [Renglon]
colocaObjeto coordenadaChar coordenadaNum  tablero  objeto =  colocaObjetoAuxiliar (numero coordenadaChar) coordenadaNum tablero objeto

colocaObjetoAuxiliar:: Int -> Int -> [Renglon] ->Int -> [Renglon]
colocaObjetoAuxiliar 1 y tablero objeto = [reemplazarRenglon y (head tablero) objeto] ++ (tail tablero )
colocaObjetoAuxiliar n y tablero objeto = [(head tablero)] ++ (colocaObjetoAuxiliar (n - 1) y (tail tablero) objeto)

reemplazarRenglon:: Int -> Renglon -> Int -> Renglon
reemplazarRenglon 1 renglon objeto = [objeto] ++ (tail renglon)
reemplazarRenglon n renglon objeto = [(head renglon)] ++ (reemplazarRenglon (n - 1) (tail renglon) objeto )

 
dibuja:: [Renglon] -> String
dibuja [] = ""
dibuja renglon = (show (head renglon)) ++"\n"++ (dibuja (tail renglon))   

linea:: Int -> String
linea 1 = "-"
linea n = "-"++(linea (n - 1 ))

casillaEnTablero:: String -> Int -> Tablero -> Bool
casillaEnTablero char n tab = casillaExistente (numero char) n tab

numero::String->Int 
numero c 
	| c == "A" = 1
	| c == "B" = 2
	| c == "C" = 3
	| c == "D" = 4
	| c == "E" = 5 
	| c == "F" = 6
	| c == "G" = 7
	| c == "H" = 8
	| c == "I" = 9
	| c == "J" = 10
	| c == "K" = 11
	| c == "L" = 12
	| c == "M" = 13
	| c == "N" = 14
	| c == "a" = 1
	| c == "b" = 2
	| c == "c" = 3
	| c == "d" = 4
	| c == "e" = 5 
	| c == "f" = 6
	| c == "g" = 7
	| c == "h" = 8
	| c == "i" = 9
	| c == "j" = 10
	| c == "k" = 11
	| c == "l" = 12
	| c == "m" = 13
	| c == "n" = 14
	| otherwise = 0